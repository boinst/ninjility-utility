@echo off

cd /d "%~dp0.\..\.."

REM ## Put NuGet on the Path
set "Path=%Path%;%CD%\src\.nuget"

REM ### Publish with Nuget
nuget.exe push .\build\nuget\packages\Ninjility.Utility.1.0.0.1.nupkg -NonInteractive
nuget.exe push .\build\nuget\packages\Ninjility.Utility.1.0.0.1.symbols.nupkg -NonInteractive

@pause