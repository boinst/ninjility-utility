@echo off

cd /d "%~dp0.\..\.."

REM ## Put MSBuild and NuGet on the Path
set "Path=%Path%;%ProgramFiles(x86)%\MSBuild\12.0\bin\amd64;%CD%\src\.nuget"

REM ### Build the library
msbuild /m /nologo /p:Configuration=Release /p:Platform="Any CPU" src\Ninjility.Utility.sln

REM ### Build with Nuget
if not exist .\build\nuget\packages mkdir .\build\nuget\packages
rm -rf .\build\nuget\packages\*

cd /d .\src\Ninjility.Utility

nuget.exe pack Ninjility.Utility.csproj -Prop Configuration=Release -OutputDirectory ..\..\build\nuget\packages -NonInteractive -Symbols

cd /d "%~dp0.\..\.."

@pause