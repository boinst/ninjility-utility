@echo off
setlocal enableextensions enabledelayedexpansion 

REM ###
REM ### This script is executed by TeamCity to produce the Windows build.
REM ###

cd /d %~dp0..\..

REM ### Build installers
C:\Tools\NAnt\nant.exe -buildfile:build/nant/build.nant -l:nant.log -D:platform=x64 -D:buildConfiguration=Release TeamCity || goto :ERROR

echo Build completed successfully
goto :END

:ERROR
echo Build FAILED with exit code %ERRORLEVEL%
exit /b %ERRORLEVEL%

:END
endlocal

:EOF
