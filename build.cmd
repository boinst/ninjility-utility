@echo off
setlocal enableextensions enabledelayedexpansion 

REM ###
REM ### Build script for the entire product.
REM ###

C:\Tools\NAnt\nant.exe -l:nant.log -buildfile:"build/nant/build.nant" -D:platform=x64 -D:buildConfiguration=Release || goto :ERROR

echo Build completed successfully
goto :END

:ERROR
echo Build FAILED with exit code %ERRORLEVEL%
@pause
exit /b %ERRORLEVEL%

:END
@pause
exit /b 0