Ninjility Utilities
============

Overview
--------

This library contains utility methods used at Ninjility.

License
-------

You may freely use and modify this library under the terms of the [MIT License](http://opensource.org/licenses/MIT).

Classes
-------

### TemporaryFile

The class `Ninjility.Utility.TemporaryFile` is a temporary file that is deleted when disposed, and if it cannot be deleted for whatever reason, it will be deleted on the next system reboot. [View the source](https://bitbucket.org/boinst/ninjility-utility/src/8d4e5dbe9a9287ae9ba7c0a9c5e8b731ffd5e937/src/Ninjility.Utility/FileSystem/TemporaryFile.cs?at=master).

### TemporaryDirectory

The class `Ninjility.Utility.TemporaryDirectory` is a temporary directory that is deleted when disposed, and if it cannot be deleted for whatever reason, it will be deleted on the next system reboot.

### AssemblyUtility

The function `RetrieveLinkerTimestamp` provides the timestamp that a PE file was linked.