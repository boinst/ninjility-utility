﻿namespace Ninjility.Utility.FileSystem
{
    using System;
    using System.IO;

    /// <summary>
    /// A temporary folder. Deletes itself when disposed or garbage collected.
    /// </summary>
    public class TemporaryFolder : IDisposable
    {
        private bool disposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="TemporaryFolder"/> class. 
        /// </summary>
        public TemporaryFolder()
        {
            // find a unique directory name that does not already exist
            do
            {
                this.Directory = new DirectoryInfo(Path.Combine(Path.GetTempPath(), Path.GetRandomFileName().Replace(".", string.Empty)));
            }
            while (this.Directory.Exists);

            // create the directory
            this.Directory.Create();
            this.Directory.Refresh();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="TemporaryFolder"/> class. 
        /// </summary>
        ~TemporaryFolder()
        {
            this.Dispose(false);
        }

        /// <summary>
        /// The DirectoryInfo object for this temporary folder
        /// </summary>
        public DirectoryInfo Directory { get; private set; }

        /// <summary>
        /// When the Temporary Folder is disposed, 
        /// it will delete the folder and and anything 
        /// in it. If the files cannot be deleted, they
        /// will be deleted on reboot.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Delete all contents of the directory.
        /// </summary>
        public void Clear()
        {
            this.Directory.Delete(true);
            this.Directory.Create();
        }

        /// <summary>
        /// thin Path.Combine wrapper: gives a full path to a relative path within the temporary folder.
        /// </summary>
        /// <param name="relpath">
        /// The relative path to get an absolute path for.
        /// </param>
        /// <returns>
        /// The absolute path of the string provided.
        /// </returns>
        public string GetFullPath(string relpath)
        {
            if (Path.IsPathRooted(relpath)) throw new Exception("An absolute path was provided, but a relative path was expected.");
            return Path.Combine(this.Directory.FullName, relpath);
        }

        /// <summary>
        /// Dispose resources
        /// </summary>
        /// <param name="disposing">
        /// "true" if called from <see cref="IDisposable.Dispose"/>, 
        /// or "false" if called from <see cref="Finalize"/>.
        /// when "false", clean up unmanaged resources only.
        /// when "true", also clean up managed resources.
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            lock (this)
            {
                if (this.disposed) return;

                this.Directory.DeleteNowOrOnReboot();
                this.Directory = null;
                this.disposed = true;
            }
        }
    }
}