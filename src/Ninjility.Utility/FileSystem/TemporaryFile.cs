﻿namespace Ninjility.Utility.FileSystem
{
    using System;
    using System.IO;

    /// <summary>
    /// A temporary file on disk.
    /// </summary>
    public class TemporaryFile : IDisposable
    {
        private readonly TemporaryFolder temporaryFolder;
        private readonly string filename;

        public TemporaryFile()
            : this(Path.GetRandomFileName())
        {
        }

        public TemporaryFile(string filename)
        {
            this.temporaryFolder = new TemporaryFolder();
            this.filename = filename;
        }

        public string FullPath
        {
            get { return this.temporaryFolder.GetFullPath(this.filename); }
        }

        public void Dispose()
        {
            this.temporaryFolder.Dispose();
        }

        public FileInfo ToFileInfo()
        {
            return new FileInfo(this.FullPath);
        }

        public override string ToString()
        {
            return this.FullPath;
        }
    }
}
