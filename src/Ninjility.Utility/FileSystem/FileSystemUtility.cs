﻿namespace Ninjility.Utility.FileSystem
{
    using System;
    using System.Diagnostics;
    using System.IO;

    /// <summary>
    /// Utility methods for manipulating file system objects.
    /// </summary>
    public static class FileSystemUtility
    {
        private const string unableToDeleteFile = "[FileSystemUtility] Unable to delete file \"{0}\": \"{1}\". Deletion will be deferred to the next boot.";
        private const string unableToDeleteDirectory = "[FileSystemUtility] Unable to delete directory \"{0}\": \"{1}\". Deletion will be deferred to the next boot.";

        /// <summary>
        /// Delete file on next reboot
        /// </summary>
        /// <param name="file">The file to delete</param>
        [DebuggerNonUserCode] // do not break if there's an exception here
        public static void DeleteNowOrOnReboot(this FileInfo file)
        {
            try
            {
                file.Delete();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(unableToDeleteFile, file.FullName, ex.Message);
                DeleteOnReboot(file.FullName);
            }
        }

        /// <summary>
        /// Delete directory on next reboot
        /// </summary>
        /// <param name="directory">The directory to delete</param>
        [DebuggerNonUserCode] // do not break if there's an exception here
        public static void DeleteNowOrOnReboot(this DirectoryInfo directory)
        {
            try
            {
                directory.Delete(true);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(unableToDeleteDirectory, directory.FullName, ex.Message);
                DeleteOnReboot(directory.FullName);
            }
        }
        
        /// <summary>
        /// Delete the object on next reboot.
        /// </summary>
        /// <param name="path">The file or directory to delete.</param>
        private static void DeleteOnReboot(string path)
        {
            MoveFileEx(path, null, MoveFileFlags.MOVEFILE_DELAY_UNTIL_REBOOT);
        }

        /// <summary>
        /// Marks the file for deletion during next system reboot
        /// </summary>
        /// <param name="srcFilename">The current name of the file or directory on the local computer.</param>
        /// <param name="destFilename">The new name of the file or directory on the local computer.</param>
        /// <param name="dwFlags">MoveFileFlags</param>
        /// <returns>"true" if successful.</returns>
        /// <remarks>http://msdn.microsoft.com/en-us/library/aa365240(VS.85).aspx</remarks>
        [System.Runtime.InteropServices.DllImportAttribute("kernel32.dll", EntryPoint = "MoveFileEx")]
        private static extern bool MoveFileEx(string srcFilename, string destFilename, MoveFileFlags dwFlags);

        /// <summary>
        /// Consts defined in WINBASE.H, used by MoveFileEx
        /// </summary>
        private enum MoveFileFlags
        {
            // ReSharper disable InconsistentNaming
            MOVEFILE_REPLACE_EXISTING = 1,
            MOVEFILE_COPY_ALLOWED = 2,
            MOVEFILE_DELAY_UNTIL_REBOOT = 4,
            MOVEFILE_WRITE_THROUGH = 8
            // ReSharper restore InconsistentNaming
        }
    }
}