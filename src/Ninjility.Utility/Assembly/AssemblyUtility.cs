﻿using System;

namespace Ninjility.Utility.Assembly
{
    using System.IO;

    public class AssemblyUtility
    {
        /// <summary>
        /// Retrieve the linker timestamp (build time) of an assembly.
        /// </summary>
        /// <remarks>
        /// This timestamp provides when the file was modified by the compiler. It's useful
        /// for determining when the file was build. "Last modified time" tends to reflect
        /// when the file was copied around.
        /// </remarks>
        /// <param name = "assemblyFileName">The filename of the assembly</param>
        /// <returns>The time of build in local time</returns>
        public static DateTime RetrieveLinkerTimestamp(string assemblyFileName)
        {
            try
            {
                const int peHeaderOffset = 60;
                const int linkerTimestampOffset = 8;
                var b = new byte[2048];
                Stream fileStream = new FileStream(assemblyFileName, FileMode.Open, FileAccess.Read);
                fileStream.Read(b, 0, 2048);
                fileStream.Close();
                int i = BitConverter.ToInt32(b, peHeaderOffset);
                int secondsSince1970 = BitConverter.ToInt32(b, (i + linkerTimestampOffset));
                var dt = new DateTime(1970, 1, 1, 0, 0, 0);
                dt = dt.AddSeconds(secondsSince1970);
                dt = dt + TimeZone.CurrentTimeZone.GetUtcOffset(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to retrieve the linker timestamp from assembly \"" + assemblyFileName + "\"", ex);
            }
        }
    }
}
