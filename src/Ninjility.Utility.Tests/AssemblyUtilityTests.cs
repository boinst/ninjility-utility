﻿namespace Ninjility.Utility.Tests
{
    using System;
    using System.IO;
    using System.Reflection;

    using Ninjility.Utility.Assembly;

    using NUnit.Framework;

    [TestFixture]
    public class AssemblyUtilityTests
    {
        [Test]
        public void Run()
        {
            var fileInfo = new FileInfo(Assembly.GetExecutingAssembly().Location);
            var modDate = File.GetLastWriteTime(fileInfo.FullName);
            var peDate = AssemblyUtility.RetrieveLinkerTimestamp(fileInfo.FullName);

            // The file modification date, and the linker date, should be essentially the same value.
            Assert.Less(Math.Abs((modDate - peDate).TotalSeconds), 1.0);
        }
    }
}
