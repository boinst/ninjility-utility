﻿namespace Ninjility.Utility.Tests
{
    using System.IO;

    using Ninjility.Utility.FileSystem;

    using NUnit.Framework;

    [TestFixture]
    public class TemporaryFolderTest
    {
        [Test]
        [Category("Unit")]
        public void CreateTest()
        {
            string path;
            using (var tf = new TemporaryFolder())
            {
                path = tf.Directory.FullName;
                Assert.IsTrue(Directory.Exists(path));
            }

            Assert.IsTrue(!Directory.Exists(path));
        }

        [Test]
        [Category("Unit")]
        public void PopulateTest()
        {
            string path;
            string filepath;
            using (var tf = new TemporaryFolder())
            {
                path = tf.Directory.FullName;
                filepath = tf.GetFullPath("testfile");

                using (var sw = new StreamWriter(filepath))
                    sw.WriteLine("Test content");

                Assert.IsTrue(Directory.Exists(path));
                Assert.IsTrue(File.Exists(filepath));
            }

            Assert.IsTrue(!Directory.Exists(path));
            Assert.IsTrue(!File.Exists(filepath));
        }

        [Test]
        [Category("Unit")]
        public void PostponeDeleteTest()
        {
            var tf = new TemporaryFolder();

            string path = tf.Directory.FullName;
            string filepath = tf.GetFullPath("testfile");

            using (var sw = new StreamWriter(filepath))
            {
                sw.WriteLine("Test content");

                // dispose the temporary folder. StreamWriter has an open handle,
                // so deletion of the folder should be postponed to the next reboot.
                tf.Dispose();

                // continue to access the file after the Temporary Folder has been disposed.
                sw.WriteLine("Some more content");
            }

            // Deletion has been postponed, so the files still exist
            Assert.IsTrue(Directory.Exists(path));
            Assert.IsTrue(File.Exists(filepath));
        }

        [Test]
        [Category("Unit")]
        public void GetFullPathTest()
        {
            var tf = new TemporaryFolder();

            var folderpath = tf.Directory.FullName;
            var filepath = tf.GetFullPath("beans");

            Assert.IsTrue(filepath.StartsWith(folderpath));
        }
    }
}