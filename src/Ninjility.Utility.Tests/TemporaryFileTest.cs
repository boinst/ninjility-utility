﻿namespace Ninjility.Utility.Tests
{
    using System.IO;

    using Ninjility.Utility.FileSystem;

    using NUnit.Framework;

    [TestFixture]
    public class TemporaryFileTest
    {
        [Test]
        [Category("Unit")]
        public void CreateTest()
        {
            string filename;
            using (var tf = new TemporaryFile("file"))
            {
                filename = tf.FullPath;
                Assert.IsFalse(File.Exists(filename));
                Assert.IsTrue((new FileInfo(filename)).Directory.Exists);

                using (var sw = new StreamWriter(filename))
                    sw.WriteLine("Test content");

                Assert.IsTrue(File.Exists(filename));
            }

            Assert.IsFalse(File.Exists(filename));
        }
    }
}
